﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;

namespace HomeReportsC
{
    public partial class PaidByDates : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetServerInfo();
            if (!IsPostBack)
                if (hfUserID.Value == "0")
                    Response.Redirect("https://carguardgenerator.com/home/default.aspx");

            if (hfError.Value == "Visible")
                rwError.VisibleOnPageLoad = true;
            else
                rwError.VisibleOnPageLoad = false;
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                {
                    btnSalesReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                {
                    btnAccountingReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                {
                    btnClaimsReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                {
                    btnCustomReports.Enabled = true;
                    btnReports.Enabled = true;
                }
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnClaimsReports.Enabled = false;
            btnSalesReports.Enabled = false;
            btnAccountingReports.Enabled = false;
            btnCustomReports.Enabled = false;
        }

        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/agents/AgentsSearch.aspx?sid=" + hfID.Value);

        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSalesReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/salesreports.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claimsreports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountingReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/AccountingReports.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            if (rdpTo.SelectedDate == null)
                return;
            if (rdpFrom.SelectedDate == null)
                return;
            SQL = @"select contractno, fname + ' ' + lname as CustName, DatePaid, SaleDate, RetailRate, 
                MoxyDealerCost as DealerCost, dealerno, dealername, pt.plantype, termmonth, cr.amt as ClaimReserve, 
                cf.amt as ClipFee, pt2.amt as PremTax, a.amt as Admin, m.amt as Markup, comm.amt as Commission, crm.amt as crmamt from contract c 
                inner join dealer d on d.dealerid = c.dealerid 
                inner join plantype pt on pt.plantypeid = c.plantypeid 
                left join (select contractid, sum(amt) - sum(cancelamt) as Amt from contractamt 
                where ratetypeid = 1 group by contractid) cr 
                on cr.contractid = c.contractid 
                left join (select contractid, sum(amt) - sum(cancelamt) as Amt from contractamt where ratetypeid = 2 group by ContractID) cf
		        on cf.ContractID = c.ContractID 
                left join (select contractid, sum(amt) - sum(cancelamt) as Amt from contractamt where ratetypeid = 3 group by ContractID) pt2
		        on pt2.ContractID = c.ContractID 
                left join (select contractid, sum(amt) - sum(cancelamt) as Amt from contractamt where ratetypeid = 4 group by ContractID) a
		        on a.ContractID = c.ContractID 
                left join (select contractid, sum(amt) - sum(cancelamt) as Amt from contractamt where ratetypeid = 5 group by ContractID) m
		        on m.ContractID = c.ContractID 
		        left join (select contractid, sum(amt) - sum(cancelamt) as Amt from contractamt ca
		        inner join ratetype rt on rt.RateTypeID = ca.RateTypeID 
		        where rt.RateCategoryID = 3 
		        and ca.RateTypeID <> 1010 
		        and ca.RateTypeID <> 2008 
		        and ca.RateTypeID <> 2010
		        group by contractid) comm
		        on comm.ContractID = c.ContractID 
		        left join (select contractid, sum(amt) - sum(cancelamt) as Amt from contractamt where (ratetypeid = 1010 or RateTypeID = 2008 or RateTypeID = 2010) group by ContractID) crm
		        on crm.ContractID = c.ContractID ";
            SQL = SQL + "where datepaid >= '" + rdpFrom.SelectedDate + "' ";
            SQL = SQL + "and datepaid <= '" + rdpTo.SelectedDate + "' ";
            rgPaid.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgPaid.Rebind();
        }

        protected void rgPaid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string a;
            a = e.CommandName;
            if (e.CommandName == "ExportToExcel")
            {
                rgPaid.ExportSettings.ExportOnlyData = false;
                rgPaid.ExportSettings.IgnorePaging = true;
                rgPaid.ExportSettings.OpenInNewWindow = true;
                rgPaid.ExportSettings.UseItemStyles = true;
                rgPaid.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                rgPaid.MasterTableView.ExportToExcel();
            }
        }
    }
}