﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;

namespace HomeReportsC
{
    public partial class RatingVerification : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetServerInfo();
            if (!IsPostBack)
                if (hfUserID.Value == "0")
                    Response.Redirect("https://carguardgenerator.com/home/default.aspx");

            FillGrid();
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                {
                    btnSalesReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                {
                    btnAccountingReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                {
                    btnClaimsReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                {
                    btnCustomReports.Enabled = true;
                    btnReports.Enabled = true;
                }
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnClaimsReports.Enabled = false;
            btnSalesReports.Enabled = false;
            btnAccountingReports.Enabled = false;
            btnCustomReports.Enabled = false;
        }

        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        private void FillGrid()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = @"select contractno, c.Status, MoxyDealerCost, CustomerCost, pt.PlanType, TermMonth, (cr.Amt - cr.CancelAmt) as ClaimReserve, (cp.amt - cp.CancelAmt) as ClipFee, 
                (tax.Amt - tax.CancelAmt) as PremiumTax, (a.Amt - a.CancelAmt) as Admin, (m.Amt - m.CancelAmt) as Markup, 
                case when (jd.Amt - jd.CancelAmt) is null then 0 else (jd.Amt - jd.CancelAmt) end  as JDecker,
                case when (mg.Amt - mg.CancelAmt) is null then 0 else (mg.Amt - mg.CancelAmt) end as mcgrp, 
                case when (gd.Amt - gd.CancelAmt) is null then 0 else (gd.Amt - gd.CancelAmt) end as GrandeCom, 
                case when (dmg.Amt - dmg.CancelAmt) is null then 0 else (dmg.Amt - dmg.CancelAmt) end as DirectMarketing, 
                case when (vsc.Amt - vsc.CancelAmt) is null then 0 else (vsc.Amt - vsc.CancelAmt) end as VSCLogistics,
                case when (f.Amt - f.CancelAmt) is null then 0 else (f.Amt - f.CancelAmt) end as ForteCRMFee, 
                case when (mcf.Amt - mcf.CancelAmt) is null then 0 else (mcf.Amt - mcf.CancelAmt) end as MoxyCRMFee, 
                case when (ap.amt - ap.CancelAmt) is null then 0 else (ap.amt - ap.CancelAmt) end as AutoProtectUSA,
                (MoxyDealerCost - case when (cr.Amt - cr.CancelAmt) is null then 0 else (cr.Amt - cr.CancelAmt) end - 
                case when (cp.amt - cp.CancelAmt) is null then 0 else (cp.amt - cp.CancelAmt) end - 
                case when (tax.Amt - tax.CancelAmt) is null then 0 else (tax.Amt - tax.CancelAmt) end - 
                case when (a.Amt - a.CancelAmt) is null then 0 else (a.Amt - a.CancelAmt) end - 
                case when (m.Amt - m.CancelAmt) is null then 0 else (m.Amt - m.CancelAmt) end -
                case when (jd.Amt - jd.CancelAmt) is null then 0 else (jd.Amt - jd.CancelAmt) end - 
                case when (mg.Amt - mg.CancelAmt) is null then 0 else (mg.Amt - mg.CancelAmt) end - 
                case when (gd.Amt - gd.CancelAmt) is null then 0 else (gd.Amt - gd.CancelAmt) end - 
                case when (dmg.Amt - dmg.CancelAmt) is null then 0 else (dmg.Amt - dmg.CancelAmt) end - 
                case when (vsc.Amt - vsc.CancelAmt) is null then 0 else (vsc.Amt - vsc.CancelAmt) end -
                case when (f.Amt - f.CancelAmt) is null then 0 else (f.Amt - f.CancelAmt) end - 
                case when (mcf.Amt - mcf.CancelAmt) is null then 0 else (mcf.Amt - mcf.CancelAmt) end - 
                case when (ap.amt - ap.CancelAmt) is null then 0 else (ap.amt - ap.CancelAmt) end) as DiffAmt
                from contract c
                inner join PlanType pt on pt.PlanTypeID = c.PlanTypeID
                left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=1) cr on cr.ContractID = c.ContractID
                left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=2) cp on cp.ContractID = c.ContractID
                left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=3) tax on tax.ContractID = c.ContractID
                left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=4) a on a.ContractID = c.ContractID
                left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=5) m on m.ContractID = c.ContractID
                left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=6) jd on jd.ContractID = c.ContractID
                left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=7) mg on mg.ContractID = c.ContractID
                left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=8) gd on gd.ContractID = c.ContractID
                left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=1008) dmg on dmg.ContractID = c.ContractID
                left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=1009) vsc on vsc.ContractID = c.ContractID
                left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=1010) f on f.ContractID = c.ContractID
                left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=2010) MCF on MCF.ContractID = c.ContractID
                left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=2014) AP on AP.ContractID = c.ContractID
                where ((MoxyDealerCost - case when (cr.Amt - cr.CancelAmt) is null then 0 else (cr.Amt - cr.CancelAmt) end - 
                case when (cp.amt - cp.CancelAmt) is null then 0 else (cp.amt - cp.CancelAmt) end - 
                case when (tax.Amt - tax.CancelAmt) is null then 0 else (tax.Amt - tax.CancelAmt) end - 
                case when (a.Amt - a.CancelAmt) is null then 0 else (a.Amt - a.CancelAmt) end - 
                case when (m.Amt - m.CancelAmt) is null then 0 else (m.Amt - m.CancelAmt) end -
                case when (jd.Amt - jd.CancelAmt) is null then 0 else (jd.Amt - jd.CancelAmt) end - 
                case when (mg.Amt - mg.CancelAmt) is null then 0 else (mg.Amt - mg.CancelAmt) end - 
                case when (gd.Amt - gd.CancelAmt) is null then 0 else (gd.Amt - gd.CancelAmt) end - 
                case when (dmg.Amt - dmg.CancelAmt) is null then 0 else (dmg.Amt - dmg.CancelAmt) end - 
                case when (vsc.Amt - vsc.CancelAmt) is null then 0 else (vsc.Amt - vsc.CancelAmt) end -
                case when (f.Amt - f.CancelAmt) is null then 0 else (f.Amt - f.CancelAmt) end - 
                case when (mcf.Amt - mcf.CancelAmt) is null then 0 else (mcf.Amt - mcf.CancelAmt) end - 
                case when (ap.amt - ap.CancelAmt) is null then 0 else (ap.amt - ap.CancelAmt) end) > 1
                or
                (MoxyDealerCost - case when (cr.Amt - cr.CancelAmt) is null then 0 else (cr.Amt - cr.CancelAmt) end - 
                case when (cp.amt - cp.CancelAmt) is null then 0 else (cp.amt - cp.CancelAmt) end - 
                case when (tax.Amt - tax.CancelAmt) is null then 0 else (tax.Amt - tax.CancelAmt) end - 
                case when (a.Amt - a.CancelAmt) is null then 0 else (a.Amt - a.CancelAmt) end - 
                case when (m.Amt - m.CancelAmt) is null then 0 else (m.Amt - m.CancelAmt) end -
                case when (jd.Amt - jd.CancelAmt) is null then 0 else (jd.Amt - jd.CancelAmt) end - 
                case when (mg.Amt - mg.CancelAmt) is null then 0 else (mg.Amt - mg.CancelAmt) end - 
                case when (gd.Amt - gd.CancelAmt) is null then 0 else (gd.Amt - gd.CancelAmt) end - 
                case when (dmg.Amt - dmg.CancelAmt) is null then 0 else (dmg.Amt - dmg.CancelAmt) end - 
                case when (vsc.Amt - vsc.CancelAmt) is null then 0 else (vsc.Amt - vsc.CancelAmt) end -
                case when (f.Amt - f.CancelAmt) is null then 0 else (f.Amt - f.CancelAmt) end - 
                case when (mcf.Amt - mcf.CancelAmt) is null then 0 else (mcf.Amt - mcf.CancelAmt) end - 
                case when (ap.amt - ap.CancelAmt) is null then 0 else (ap.amt - ap.CancelAmt) end) < -1) ";

            rgPaid.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgPaid.Rebind();
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/dealer/dealersearch.aspx?sid=" + hfID.Value);

        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSalesReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/salesreports.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claimsreports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountingReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/AccountingReports.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void rgPaid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string a;
            a = e.CommandName;
            if (e.CommandName == "ExportToExcel")
            {
                rgPaid.ExportSettings.ExportOnlyData = false;
                rgPaid.ExportSettings.IgnorePaging = true;
                rgPaid.ExportSettings.OpenInNewWindow = true;
                rgPaid.ExportSettings.UseItemStyles = true;
                rgPaid.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                rgPaid.MasterTableView.ExportToExcel();
            }
        }
    }
}