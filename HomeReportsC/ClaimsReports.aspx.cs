﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;

namespace HomeReportsC
{
    public partial class ClaimsReports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            GetServerInfo();
            CheckToDo();
            if (!IsPostBack)
            {
                if (Convert.ToInt32(hfUserID.Value) == 0)
                    Response.Redirect("https://carguardgenerator.com/home/default.aspx");
                if (ConfigurationManager.AppSettings["connstring"].Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                }

                FillReports();
            }

            if (hfError.Value == "Visible")
                rwError.VisibleOnPageLoad = true;
            else
                rwError.VisibleOnPageLoad = false;
        }

     

        private void FillReports()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select reportid, reportno, reportname from reports where reportcategory = 'Claims' ";

            rgReport.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgReport.Rebind();
        }

        
        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "https://carguardgenerator.com/home/users/todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO clTD = new clsDBO();
            SQL = "select * from usermessage ";
            SQL = SQL + "where toid = " + hfUserID.Value + " ";
            SQL = SQL + "and completedmessage = 0 ";
            SQL = SQL + "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clTD.RowCount() > 0)
                hlToDo.Visible = true;
            else
                hlToDo.Visible = false;
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                {
                    btnSalesReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                {
                    btnAccountingReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                {
                    btnClaimsReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                {
                    btnCustomReports.Enabled = true;
                    btnReports.Enabled = true;
                }
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnClaimsReports.Enabled = false;
            btnSalesReports.Enabled = false;
            btnAccountingReports.Enabled = false;
            btnCustomReports.Enabled = false;
        }

        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSalesReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/salesreports.aspx?sid=" + hfID.Value);
        }

        protected void rgReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            int value = Convert.ToInt32(rgReport.SelectedValue);
            if (value == 3)
                Response.Redirect("http://veritasgeneratorreports.com/reports/claimsreports/ClaimsOpen.html");
            if (value == 1006)
                Response.Redirect("~/reports/claimsreports/ClaimStats.aspx?sid=" + hfID.Value);
            if (value == 1007)
                Response.Redirect("~/reports/claimsreports/ClaimPendingPaymentAge.aspx?sid=" + hfID.Value);
            if (value == 1011)
                Response.Redirect("~/reports/claimsreports/ANLossCode.aspx?sid=" + hfID.Value);
            if (value == 1012)
                Response.Redirect("~/reports/claimsreports/SingleMultipleClaim.aspx?sid=" + hfID.Value);
            if (value == 1017)
                Response.Redirect("~/reports/claimsreports/PaidDenied.aspx?sid=" + hfID.Value);
            if (value == 1018)
                Response.Redirect("~/reports/claimsreports/ANClaim.aspx?sid=" + hfID.Value);
            if (value == 1020)
                Response.Redirect("~/reports/claimsreports/ClaimApproveMonth.aspx?sid=" + hfID.Value);
            if (value == 1021)
                Response.Redirect("~/reports/claimsreports/ClaimApprovedWeek.aspx?sid=" + hfID.Value);
            if (value == 1022)
                Response.Redirect("~/reports/claimsreports/ClaimPaidReport.aspx?sid=" + hfID.Value);
            if (value == 1026)
                Response.Redirect("~/reports/claimsreports/ClaimApproveDaily.aspx?sid=" + hfID.Value);
            if (value == 1027)
                Response.Redirect("~/reports/claimsreports/ClaimsOpenAll.aspx?sid=" + hfID.Value);
            if (value == 1028)
                Response.Redirect("~/reports/claimsreports/OpenedClaims.aspx?sid=" + hfID.Value);
            if (value == 1029)
                Response.Redirect("~/reports/claimsreports/ClaimAccess.aspx?sid=" + hfID.Value);
            if (value == 1030)
                Response.Redirect("~/reports/claimsreports/ClaimAdjusterRank.aspx?sid=" + hfID.Value);
            if (value == 1031)
                Response.Redirect("~/reports/claimsreports/ClaimsOpenLive.aspx?sid=" + hfID.Value);
            if (value == 1032)
                Response.Redirect("~/reports/claimsreports/ANClaimOpenLive.aspx?sid=" + hfID.Value);
            if (value == 1033)
                Response.Redirect("~/reports/claimsreports/ClaimOpenApprovedLive.aspx?sid=" + hfID.Value);
            if ( value == 1034)
                Response.Redirect("~/reports/claimsreports/ANClaimOpenApprovedLive.aspx?sid=" + hfID.Value);
            if (value == 1035)
                Response.Redirect("~/reports/claimsreports/ClaimOpenLiveByTeams.aspx?sid=" + hfID.Value);
            if (value == 1036)
                Response.Redirect("~/reports/claimsreports/ANClaimOpenLiveByTeam.aspx?sid=" + hfID.Value);
            if (value == 1037)
                Response.Redirect("~/reports/claimsreports/ClaimOpenApproveLiveByTeam.aspx?sid=" + hfID.Value);
            if (value == 1038)
                Response.Redirect("~/reports/claimsreports/ANClaimOpenApproveLiveByTeam.aspx?sid=" + hfID.Value);
            if (value == 1039)
                Response.Redirect("~/reports/claimsreports/MonthEndClaimReserveCMF.aspx?sid=" + hfID.Value);
            if (value == 1040)
                Response.Redirect("~/reports/claimsreports/ClaimDataPoint.aspx?sid=" + hfID.Value);
            if (value == 1041)
                Response.Redirect("~/reports/claimsreports/ServiceCenterClaim.aspx?sid=" + hfID.Value);
            if (value == 1042)
                Response.Redirect("~/reports/claimsreports/VeroClaimOpenLive.aspx?sid=" + hfID.Value);
            if (value == 1043)
                Response.Redirect("~/reports/claimsreports/ClaimAuthorized.aspx?sid=" + hfID.Value);
            if (value == 1044)
                Response.Redirect("~/reports/claimsreports/ClaimsOnline.aspx?sid=" + hfID.Value);
            if (value == 1045)
                Response.Redirect("~/reports/claimsreports/ClaimCMF.aspx?sid=" + hfID.Value);
            if (value == 1046)
                Response.Redirect("~/reports/claimsreports/SpecialClaimOpen.aspx?sid=" + hfID.Value);
            if (value == 1047)
                Response.Redirect("~/reports/claimsreports/OpenInspection.aspx?sid=" + hfID.Value);
            if (value == 1048)
                Response.Redirect("~/reports/claimsreports/WebClaimOpen.aspx?sid=" + hfID.Value);
            if (value == 1049)
                Response.Redirect("~/reports/claimsreports/ANClaimBalance.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountingReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/AccountingReports.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/default.aspx");
        }

        protected void btnClaimsReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claimsreports.aspx?sid=" + hfID.Value);
        }
    }
}