﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;

namespace HomeReportsC
{
    public partial class SalesReports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            SqlDataSource1.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            GetServerInfo();
            CheckToDo();
            if (!IsPostBack)
            {
                if (Convert.ToInt32(hfUserID.Value) == 0)
                    Response.Redirect("https://carguardgenerator.com/home/default.aspx");
                if (ConfigurationManager.AppSettings["connstring"].Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                }
            }

            if (hfError.Value == "Visible")
                rwError.VisibleOnPageLoad = true;
            else
                rwError.VisibleOnPageLoad = false;
        }

        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "https://carguardgenerator.com/home/users/todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO clTD = new clsDBO();
            SQL = "select * from usermessage ";
            SQL = SQL + "where toid = " + hfUserID.Value + " ";
            SQL = SQL + "and completedmessage = 0 ";
            SQL = SQL + "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clTD.RowCount() > 0)
                hlToDo.Visible = true;
            else
                hlToDo.Visible = false;
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                {
                    btnSalesReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                {
                    btnAccountingReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                {
                    btnClaimsReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                {
                    btnCustomReports.Enabled = true;
                    btnReports.Enabled = true;
                }
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnClaimsReports.Enabled = false;
            btnSalesReports.Enabled = false;
            btnAccountingReports.Enabled = false;
            btnCustomReports.Enabled = false;
        }

        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/default.aspx");

        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSalesReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/salesreports.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claimsreports.aspx?sid=" + hfID.Value);
        }

        protected void rgSalesReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            int value = Convert.ToInt32(rgSalesReport.SelectedValue);
            if (value == 1)
                Response.Redirect("~/reports/salesreports/reportR0001.aspx?sid=" + hfID.Value + "&ReportID=" + rgSalesReport.SelectedValue);
            if (value == 2)
                Response.Redirect("~/reports/salesreports/reportR0002.aspx?sid=" + hfID.Value + "&ReportID=" + rgSalesReport.SelectedValue);
            if (value == 1003)
                Response.Redirect("~/reports/salesreports/r0004.aspx?sid=" + hfID.Value);
            if (value == 1004)
                Response.Redirect("~/reports/salesreports/r0005.aspx?sid=" + hfID.Value);
            if (value == 1005)
                Response.Redirect("~/reports/salesreports/r0006.aspx?sid=" + hfID.Value);
            if (value == 1008)
                Response.Redirect("http://veritasgeneratorreports.com/reports/salesreports/NotPaidReport.html");
            if (value == 1009)
                Response.Redirect("~/reports/salesreports/r0010.aspx?sid=" + hfID.Value);
            if (value == 1013)
                Response.Redirect("~/reports/salesreports/Cancellations.aspx?sid=" + hfID.Value);
            if (value == 1016)
                Response.Redirect("~/reports/salesreports/PaidCancel.aspx?sid=" + hfID.Value);
            if (value == 1019)
                Response.Redirect("~/reports/salesreports/r0020.aspx?sid=" + hfID.Value);
            if (value == 1023)
                Response.Redirect("~/reports/salesreports/ContractSale.aspx?sid=" + hfID.Value);
            if (value == 1024)
                Response.Redirect("~/reports/salesreports/ContractsPaid.aspx?sid=" + hfID.Value);
            if (value == 1025)
                Response.Redirect("~/reports/salesreports/ContractBucket.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }
        protected void btnAccountingReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/AccountingReports.aspx?sid=" + hfID.Value);
        }
        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://carguardgenerator.com/home/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        
    }
}